//
//  BaseAPIService.m
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import "BaseAPIService.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "SAMKeychain.h"

#define kContentTypeKey @"Content-Type"
#define kApiKeyKey @"apikey"
#define kAuthTokenKey @"access-token"
#define kContentType @"application/json; charset=utf-8"

@implementation BaseAPIService

+ (NSMutableURLRequest*)getLoginRequestForUsername:(NSString*)username password:(NSString*)password
{
    NSString* requestURL = [NSString stringWithFormat:@"%@%@", kBaseAPIURL, kSignInAPIURL];

    NSDictionary* params = @{ @"organization" : @"clover",
        @"username" : username,
        @"password" : password
    };

    NSMutableURLRequest* req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:requestURL parameters:params error:nil];
    [req setValue:kAPIKey forHTTPHeaderField:kApiKeyKey];
    [req setValue:kContentType forHTTPHeaderField:kContentTypeKey];

    return req;
}

+ (NSMutableURLRequest*)getAPIRequestForMessage:(NSString*)message
{
    NSString* requestURL = [NSString stringWithFormat:@"%@%@", kBaseAPIURL, kMessageAPIURL];

    NSDictionary* params = @{ @"targetType" : @"3",
        @"messageType" : @"1",
        @"target" : @"5a05ccd4829e64fd1dcd7732",
        @"message" : message
    };

    NSMutableURLRequest* req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:requestURL parameters:params error:nil];
    [req setValue:kAPIKey forHTTPHeaderField:kApiKeyKey];
    [req setValue:kContentType forHTTPHeaderField:kContentTypeKey];
    NSString* authToken = [self getAuthToken];
    [req setValue:authToken forHTTPHeaderField:kAuthTokenKey];

    return req;
}

+ (NSString*)getAuthToken
{
    NSString* username = [[NSUserDefaults standardUserDefaults] stringForKey:kUsernameKey];
    return [SAMKeychain passwordForService:kAPIKey account:username];
}

+ (NSString*)loginErrorMessage
{
    return @"Sorry, something went wrong with login. Please try again later.";
}

+ (NSString*)sendMessageErrorMessage
{
    return @"Sorry, you cannot send messages at the moment. Please try again later.";
}

@end
