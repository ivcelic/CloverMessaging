//
//  MessagingService.h
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import "BaseAPIService.h"

@interface MessagingService : BaseAPIService

+ (void)sendMessage:(NSString*)message success:(void (^)())success failure:(void (^)(NSString* errorResponse))failure;

@end
