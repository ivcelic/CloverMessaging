//
//  AuthenticationService.h
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseAPIService.h"

@interface AuthenticationService : BaseAPIService

+ (void)loginWithUsername:(NSString*)username password:(NSString*)password success:(void (^)(NSString* apiResponse))success failure:(void (^)(NSString* errorResponse))failure;

@end
