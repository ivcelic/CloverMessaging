//
//  main.m
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
