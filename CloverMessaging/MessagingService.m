//
//  MessagingService.m
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import "MessagingService.h"
#import "AFNetworking.h"

@implementation MessagingService

+ (void)sendMessage:(NSString*)message success:(void (^)())success failure:(void (^)(NSString* errorResponse))failure
{
    AFURLSessionManager* manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSURLRequest* request = [BaseAPIService getAPIRequestForMessage:message];
    
    [[manager dataTaskWithRequest:request
                completionHandler:^(NSURLResponse* _Nonnull response, id _Nullable responseObject, NSError* _Nullable error) {
                    if (!error) {
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                        if(httpResponse.statusCode == 200) {
                            success();
                        } else {
                            failure([BaseAPIService sendMessageErrorMessage]);
                        }
                    } else {
                        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData*)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                        NSLog(@"Request %@ on API failed. Error response: %@", [request URL], ErrorResponse);
                        failure(ErrorResponse);
                    }
                }] resume];
}

@end
