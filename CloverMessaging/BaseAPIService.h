//
//  BaseAPIService.h
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString* const kSignInAPIURL = @"/signin";
static NSString* const kMessageAPIURL = @"/messages";

@interface BaseAPIService : NSObject

+ (NSMutableURLRequest*)getLoginRequestForUsername:(NSString*)username password:(NSString*)password;
+ (NSMutableURLRequest*)getAPIRequestForMessage:(NSString*)message;
+ (NSString*)loginErrorMessage;
+ (NSString*)sendMessageErrorMessage;

@end
