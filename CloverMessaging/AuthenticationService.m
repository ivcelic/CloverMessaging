//
//  AuthenticationService.m
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import "AuthenticationService.h"
#import "AFNetworking.h"
#import "Constants.h"

@implementation AuthenticationService

+ (void)loginWithUsername:(NSString*)username password:(NSString*)password success:(void (^)(NSString* apiResponse))success failure:(void (^)(NSString* errorResponse))failure
{
    username = @"jobapplicant";
    password = @"pQw4md4YZR";

    AFURLSessionManager* manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    NSURLRequest* request = [BaseAPIService getLoginRequestForUsername:username password:password];

    [[manager dataTaskWithRequest:request
                completionHandler:^(NSURLResponse* _Nonnull response, id _Nullable responseObject, NSError* _Nullable error) {
                    if (!error) {
                        NSError* error;
                        NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                             options:kNilOptions
                                                                               error:&error];
                        NSString* token = dict[@"access-token"];
                        if (token.length > 0) {
                            success(token);
                        } else {
                            failure([BaseAPIService loginErrorMessage]);
                        }
                    } else {
                        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData*)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                        NSLog(@"Request %@ on API failed. Error response: %@", [request URL], ErrorResponse);
                        failure(ErrorResponse);
                    }
                }] resume];
}

@end
