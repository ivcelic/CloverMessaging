//
//  LoginViewController.m
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import "LoginViewController.h"
#import "AuthenticationService.h"
#import "SAMKeychain.h"
#import "Constants.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField* usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField* passwordTextField;
- (IBAction)loginButtonClicked:(UIButton*)sender;
@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self.view endEditing:YES];
    [self clearTextFields];
    [super viewWillDisappear:animated];
}

- (void)clearTextFields
{
    _usernameTextField.text = @"";
    _passwordTextField.text = @"";
}

- (IBAction)loginButtonClicked:(UIButton*)sender
{
    if (_usernameTextField.text.length > 0 && _passwordTextField.text.length > 0) {
        [self loginWithUsername:_usernameTextField.text password:_passwordTextField.text];
    } else {
        [self displayToastMessage:@"Please fill in all required fields."];
    }
}

- (void)loginWithUsername:(NSString*)username password:(NSString*)password
{
    [self.view endEditing:YES];
    [self showProgressDialog];
    [AuthenticationService loginWithUsername:username
        password:password
        success:^(NSString* apiResponse) {
            [self hideProgressDialog];
            [[NSUserDefaults standardUserDefaults] setObject:username forKey:kUsernameKey];
            [SAMKeychain setPassword:apiResponse forService:kAPIKey account:username];
            [self performSegueWithIdentifier:@"sendMessage" sender:self];
        }
        failure:^(NSString* errorResponse) {
            [self hideProgressDialog];
            [self displayToastMessage:errorResponse];
        }];
}

@end
