//
//  BaseViewController.h
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController <UITextFieldDelegate>

- (void)showProgressDialog;
- (void)hideProgressDialog;
- (void)displayToastMessage:(NSString*)message;

@end
