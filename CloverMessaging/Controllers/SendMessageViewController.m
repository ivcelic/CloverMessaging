
//
//  SendMessageViewController.m
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import "SendMessageViewController.h"
#import "MessagingService.h"

#define kMessageTextViewPlaceholder @"Type your message here..."

@interface SendMessageViewController ()
@property (weak, nonatomic) IBOutlet UITextView* messageTextView;
- (IBAction)sendMessageButtonClicked:(UIButton*)sender;
@end

@implementation SendMessageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setViewElements];
}

- (void)setViewElements
{
    _messageTextView.text = kMessageTextViewPlaceholder;
    [self setTransparentNavigationBar];
}

- (void)setTransparentNavigationBar
{
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
}

- (void)textViewDidBeginEditing:(UITextView*)textView
{
    if ([textView.text isEqual:kMessageTextViewPlaceholder]) {
        textView.text = @"";
    }
}

- (void)textViewDidEndEditing:(UITextView*)textView
{
    if ([textView.text isEqual:@""]) {
        textView.text = kMessageTextViewPlaceholder;
    }
}

- (IBAction)sendMessageButtonClicked:(UIButton*)sender
{
    if ([_messageTextView.text isEqual:kMessageTextViewPlaceholder]) {
        [self displayToastMessage:@"Please write a message that you would like to send."];
    } else {
        [self sendMessage:_messageTextView.text];
    }
}

- (void)sendMessage:(NSString*)message
{
    self.view.userInteractionEnabled = NO;
    [MessagingService sendMessage:message
        success:^{
            [self displayToastMessage:@"Message successfully sent."];
            self.view.userInteractionEnabled = YES;
        }
        failure:^(NSString* errorResponse) {
            [self displayToastMessage:errorResponse];
            self.view.userInteractionEnabled = YES;
        }];
}

@end
