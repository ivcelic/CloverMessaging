//
//  LoginViewController.h
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface LoginViewController : BaseViewController

@end
