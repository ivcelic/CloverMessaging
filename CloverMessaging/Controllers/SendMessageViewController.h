//
//  SendMessageViewController.h
//  CloverMessaging
//
//  Created by Iris on 20/11/2017.
//  Copyright © 2017 Iris Veronika Celic. All rights reserved.
//

#import "BaseViewController.h"

@interface SendMessageViewController : BaseViewController <UITextViewDelegate>

@end
